﻿Imports System
Imports System.IO
Imports System.Windows.Forms

Friend Module modProgram

    Friend Sub Main()

        ' Initialize SFML Stuff
        SFML.Portable.Activate()
        Graphics.Initialize()
        Audio.Initialize()

        frmMain.Show()

        While True
            Application.DoEvents()
        End While

    End Sub

    Friend Sub Terminate()

        Graphics.Destroy()
        Audio.Destroy()
        Environment.Exit(0)
    End Sub

    Friend Sub SetTile(x As Integer, y As Integer)

        If x < 0 OrElse x > Map.MaxX OrElse y < 0 OrElse y > Map.MaxY Then Return

        Dim xIter As Integer = 0, yIter As Integer = 0
        Dim xCount As Integer = If(x + EditorSelectX > Map.MaxX, Map.MaxX - x, EditorSelectX)
        Dim yCount As Integer = If(y + EditorSelectY > Map.MaxY, Map.MaxY - y, EditorSelectY)

        Do While yIter <= yCount

            Do While xIter <= xCount

                Map.Layer(EditorZ).Tile(x + xIter, y + yIter)(EditorLayer).Tileset = CShort(EditorTileset)
                Map.Layer(EditorZ).Tile(x + xIter, y + yIter)(EditorLayer).X = CShort(EditorX + xIter)
                Map.Layer(EditorZ).Tile(x + xIter, y + yIter)(EditorLayer).Y = CShort(EditorY + yIter)

                xIter += 1

            Loop : xIter = 0

            yIter += 1

        Loop

    End Sub

    Friend Sub ClearTile(x As Integer, y As Integer)

        If x < 0 OrElse x > Map.MaxX OrElse
            y < 0 OrElse y > Map.MaxY Then Return

        Map.Layer(EditorZ).Tile(x, y)(EditorLayer).Tileset = 0
        Map.Layer(EditorZ).Tile(x, y)(EditorLayer).X = 0
        Map.Layer(EditorZ).Tile(x, y)(EditorLayer).Y = 0

        EditorSelectX = 0 : EditorSelectY = 0

    End Sub

    Friend Sub RelistMaps()

        Dim di As New IO.DirectoryInfo(Path.Map)
        Dim fi As FileInfo() = di.GetFiles("*.map")

        frmMain.lstIndex.Items.Clear()
        For Each f As FileInfo In fi
            frmMain.lstIndex.Items.Add(f.Name.Substring(0, f.Name.Length - 4))
        Next

    End Sub

    Friend Sub ResetPropertyWindow()

        frmMain.rsMap.Width = Map.MaxX * TileSize + TileSize
        frmMain.rsMap.Height = Map.MaxY * TileSize + TileSize

        frmMain.scrlSizeX.Value = Map.MaxX
        frmMain.lblSizeX.Text = "X: " & (Map.MaxX + 1)

        frmMain.scrlSizeY.Value = Map.MaxY
        frmMain.lblSizeY.Text = "Y: " & (Map.MaxY + 1)

        frmMain.scrlSizeZ.Value = Map.MaxZ
        frmMain.lblSizeZ.Text = "Z: " & (Map.MaxZ + 1)

        frmMain.scrlEditLayer.Value = EditorLayer
        frmMain.lblEditLayer.Text = "Layer: " & EditorLayer.ToString()

        frmMain.scrlEditZ.Maximum = 5
        frmMain.scrlEditZ.Value = EditorZ
        frmMain.scrlEditZ.Maximum = Map.MaxZ
        frmMain.lblEditZ.Text = "Z: " & (EditorZ + 1)

    End Sub

End Module

﻿Imports System

Namespace Path
    Friend Module modPathways

        Friend Function Map() As String
            Return Environment.CurrentDirectory & "\Contents\Map\"
        End Function

        Friend Function Graphics() As String
            Return Environment.CurrentDirectory & "\Contents\Graphics\"
        End Function

        Friend Function Music() As String
            Return Environment.CurrentDirectory & "\Contents\Music\"
        End Function

        Friend Function Sound() As String
            Return Environment.CurrentDirectory & "\Contents\Sound\"
        End Function

    End Module
End Namespace
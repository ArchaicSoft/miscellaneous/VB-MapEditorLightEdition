﻿Imports ASFW

Friend Structure MapDef

    Friend Layer As LayerDef()

    Friend Name As String
    Friend Music As String
    Friend Comment As String

    Friend Revision As Integer

    Friend MaxX As Short
    Friend MaxY As Short
    Friend MaxZ As Short

    Friend Sub New(x As Integer, y As Integer, z As Integer)

        ReDim Layer(z + 1)
        For l As Integer = 0 To z
            Layer(l) = New LayerDef(x + 1, y + 1)
        Next

        Name = "New Map"
        Music = ""
        Comment = ""

        MaxX = CShort(x)
        MaxY = CShort(y)
        MaxZ = CShort(z)

    End Sub

    Friend Sub Load(path As String)

        Dim stream As New ByteStream(4)
        ASFW.IO.FileIO.BinaryFile.Load(path, stream)

        Name = stream.ReadString
        Music = stream.ReadString
        Comment = stream.ReadString

        Revision = stream.ReadInt32

        MaxX = stream.ReadInt16
        MaxY = stream.ReadInt16
        MaxZ = stream.ReadInt16

        ReDim Layer(MaxZ + 1)
        For z As Integer = 0 To MaxZ
            Layer(z) = New LayerDef(MaxX + 1, MaxY + 1)

            For y As Integer = 0 To MaxY
                For x As Integer = 0 To MaxX
                    For l As Integer = 0 To LayerType.Count - 1
                        Layer(z).Tile(x, y)(l).Tileset = stream.ReadInt16
                        Layer(z).Tile(x, y)(l).X = stream.ReadInt16
                        Layer(z).Tile(x, y)(l).Y = stream.ReadInt16
                    Next
                Next
            Next
        Next

    End Sub

    Friend Sub Save(path As String)

        Dim stream As New ByteStream(4)

        stream.WriteString(Name)
        stream.WriteString(Music)
        stream.WriteString(Comment)

        stream.WriteInt32(Revision)

        stream.WriteInt16(MaxX)
        stream.WriteInt16(MaxY)
        stream.WriteInt16(MaxZ)

        For z As Integer = 0 To MaxZ
            For y As Integer = 0 To MaxY
                For x As Integer = 0 To MaxX
                    For l As Integer = 0 To LayerType.Count - 1
                        stream.WriteInt16(Layer(z).Tile(x, y)(l).Tileset)
                        stream.WriteInt16(Layer(z).Tile(x, y)(l).X)
                        stream.WriteInt16(Layer(z).Tile(x, y)(l).Y)
                    Next
                Next
            Next
        Next

        ASFW.IO.FileIO.BinaryFile.Save(path, stream)

    End Sub

    Friend Sub Resize(x As Integer, y As Integer, z As Integer)

        Dim tLayer As LayerDef()
        ReDim tLayer(z + 1)

        For l As Integer = 0 To z
            tLayer(l) = New LayerDef(x + 1, y + 1)
        Next
        
        Dim _x As Integer = If(x > MaxX, MaxX, x)
        Dim _y As Integer = If(y > MaxY, MaxY, y)
        Dim _z As Integer = If(z > MaxZ, MaxZ, z)

        For tZ As Integer = 0 To _z
            For tY As Integer = 0 To _y
                For tX As Integer = 0 To _x
                    tLayer(tZ).Tile(tX, tY) = Layer(tZ).Tile(tX, tY)
                Next
            Next
        Next

        Layer = tLayer
        MaxX = CShort(x)
        MaxY = CShort(y)
        MaxZ = CShort(z)

    End Sub

    Friend Sub Shift(dir As DirectionType, z As Integer)

        Dim tmpTile As TextureDef(,)()
        ReDim tmpTile(MaxX + 1, MaxY + 1)

        For y As Integer = 0 To Map.MaxY
            For x As Integer = 0 To Map.MaxX
                Select Case dir
                    Case DirectionType.Up : If y < MaxY Then tmpTile(x, y) = Layer(z).Tile(x, y + 1)
                    Case DirectionType.Left : If x <> MaxX Then tmpTile(x, y) = Layer(z).Tile(x + 1, y)
                    Case DirectionType.Right : If x > 0 Then tmpTile(x, y) = Layer(z).Tile(x - 1, y)
                    Case DirectionType.Down : If y > 0 Then tmpTile(x, y) = Layer(z).Tile(x, y - 1)
                End Select
            Next
        Next

        Layer(z).Tile = tmpTile

    End Sub

End Structure

Friend Structure LayerDef

    Friend Tile As TextureDef(,)()

    Friend Sub New(x As Integer, y As Integer)

        ReDim Tile(x + 1, y + 1)
        
        For ty As Integer = 0 To y + 1
            For tx As Integer = 0 To x + 1
                redim Tile(tx,ty)(LayerType.Count)
            Next
        Next        

    End Sub

End Structure

Friend Structure TextureDef

    Friend Tileset As Short
    Friend X As Short
    Friend Y As Short

End Structure
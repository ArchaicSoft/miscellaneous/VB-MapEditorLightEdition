﻿Imports SFML.Graphics

Namespace Graphics
    Friend Module modGraphics

        Friend Tileset As Sprite()

        Friend Sub Initialize()

            LoadStack(Tileset, Path.Graphics & "Tilesets")

        End Sub

        Friend Sub Destroy()

            UnloadStack(Tileset)

        End Sub

        Private Sub LoadStack(ByRef spr As Sprite(), path As string)

            spr = SFML.Portable.AutoSpriteStack(path)

        End Sub

        Private Sub UnloadStack(ByRef spr As Sprite())

            For i As integer = 0 To spr.Length - 1
                spr(i).Dispose()
            Next

            spr = Nothing

        End Sub

    End Module
End Namespace
﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports SFML.Audio

Namespace Audio
    Friend Module modAudio

        Private SoundPlayer As Dictionary(Of Integer, Sound)
        Private MusicPlayer As Music
        Private CurMusic As String

        Friend Sub Initialize()

            SoundPlayer = New Dictionary(Of Integer, Sound)

        End Sub

        Friend Sub Destroy()

            StopMusic()
            StopSound()

        End Sub

        Friend Sub PlayMusic(ByVal FileName As String, Optional ByVal Looped As Boolean = True)

            If Not MusicPlayer Is Nothing Then

                If MusicPlayer.Status = SoundStatus.Paused Then

                    If CurMusic = FileName AndAlso File.Exists(Path.Music & FileName) Then
                        MusicPlayer.Play()
                        Exit Sub
                    End If

                End If

            End If

            If CurMusic = FileName OrElse Not File.Exists(Path.Music & FileName) Then Exit Sub
            If Not MusicPlayer Is Nothing Then StopMusic()

            MusicPlayer = New Music(Path.Music & FileName)
            MusicPlayer.Volume() = 1
            MusicPlayer.Loop() = Looped
            MusicPlayer.Play()
            CurMusic = FileName

        End Sub

        Friend Sub SetMusicVolume(ByVal volume As Single)

            SyncLock MusicPlayer

                MusicPlayer.Volume = volume

            End SyncLock

        End Sub

        Friend Sub StopMusic()

            If MusicPlayer Is Nothing Then Exit Sub

            MusicPlayer.Stop()
            MusicPlayer.Dispose()
            MusicPlayer = Nothing
            CurMusic = ""

        End Sub

        Friend Sub PlaySound(ByVal FileName As String, Optional ByVal Looped As Boolean = False)

            If SoundPlayer Is Nothing OrElse Not File.Exists(Path.Sound & FileName) Then Exit Sub

            ClearDeadSounds()

            Dim buffer As New Sound(New SoundBuffer(Environment.CurrentDirectory & Path.Sound & FileName))
            buffer.Loop() = Looped
            buffer.Volume() = 1
            buffer.Play()
            SoundPlayer.Add(SoundPlayer.Count, buffer)

        End Sub

        Friend Sub SetSoundVolume(ByVal volume As Single)

            SyncLock SoundPlayer

                For Each snd As Sound In SoundPlayer.Values
                    snd.Volume = volume
                Next

            End SyncLock

        End Sub

        Friend Sub StopSound()

            If SoundPlayer Is Nothing Then Exit Sub

            For Each snd As Sound In SoundPlayer.Values
                snd.Stop()
                snd.Dispose()
                snd = Nothing
            Next

            SoundPlayer.Clear()

        End Sub

        Friend Sub ClearDeadSounds()

            If SoundPlayer Is Nothing Then Exit Sub

            Dim deadSounds As New List(Of Integer)
            Dim count As Integer = SoundPlayer.Count

            For i As Integer = count To 0 Step -1

                If SoundPlayer(i) Is Nothing Then

                    SoundPlayer.Remove(i)

                ElseIf SoundPlayer(i).Status = SoundStatus.Stopped Then

                    SoundPlayer(i).Stop()
                    SoundPlayer(i).Dispose()
                    SoundPlayer(i) = Nothing
                    SoundPlayer.Remove(i)
                End If
            Next

        End Sub

    End Module
End Namespace